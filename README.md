<p align="left">
    <img alt="Finalverse logo on a screenshot" src="https://finalverse.org/wp-content/uploads/2021/11/logo-1.png">
	<img alt="Finalverse logo on a screenshot" src="https://finalverse.org/wp-content/uploads/2021/11/hero-bg-2.png">
</p>

[![pipeline status](https://gitlab.com/Finalverse/finalverse/badges/master/pipeline.svg)](https://gitlab.com/Finalverse/finalverse/commits/master)
[![coverage report](https://gitlab.com/Finalverse/finalverse/badges/master/coverage.svg)](https://gitlab.com/Finalverse/finalverse/commits/master)
[![license](https://img.shields.io/github/license/veloren/veloren.svg)](https://gitlab.com/Finalverse/finalverse/blob/master/LICENSE)
[![contributor count](https://img.shields.io/github/contributors/veloren/veloren)](https://gitlab.com/Finalverse/finalverse/-/graphs/master)

## Welcome To Finalverse!

Finalverse is a multiplayer voxel RPG written in Rust. Finalverse takes inspiration from games such as Cube World, Minecraft and Dwarf Fortress. The game is currently under heavy development, but is playable.

## Useful Links

[Sign Up](https://finalverse.org/register/) - Here you can create an online account for Finalverse.
This will be needed to play on auth-enabled servers, including the official server.

#### Official social media and websites

- [Website](https://finalverse.org)

## Get Finalverse

We provide 64-bit builds for Linux, Mac, and Windows, which can be downloaded on the official website:
[https://finalverse.org](https://finalverse.org)

Due to rapid development stable versions become outdated fast and might be **incompatible with the public server**.

## F.A.Q.

### **Q:** How is this game licensed?

**A:** **It's free to play, modify and distribute. Forever.** Since it is a community project, we decided to license it under the **GNU GPL 3.0** license which means it will always stay free and open source.

### **Q:** What platforms are supported?

**A:** Finalverse can run on Windows, Linux and Mac OS on all architectures (although x86_64 is our main focus). It's probably possible to compile Finalverse on/for BSD, Fuchsia and others as well.

## Credit

Many thanks to everyone that has contributed to Finalverse's development, provided ideas, crafted art, composed music, hunted bugs, created tools and supported the project.
